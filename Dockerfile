FROM centos:6
MAINTAINER SHANYULONG
RUN rm -rf /etc/yum.repos.d/*
COPY CentOS-Base.repo epel.repo /etc/yum.repos.d/
RUN rpm --rebuilddb && yum install -y gcc gcc-c++ make openssl-devel pcre-devel 
ADD nginx-1.16.1.tar.gz /tmp
RUN cd /tmp/nginx-1.16.1 && \
    ./configure --prefix=/usr/local/nginx && \
    make -j 2 && make install && \
    rm -rf /tmp/nginx-1.16.1
COPY nginx.conf /usr/local/nginx/conf
EXPOSE 80
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]

